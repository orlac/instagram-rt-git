'use strict';

var gulp = require('gulp');


function isOnlyChange(event) {
  return event.type === 'changed';
}

module.exports = function(options) {
  gulp.task('watch', function () {

    gulp.watch([options.src + '/*.html', 'bower.json'], ['build']);

    gulp.watch(options.src + '/{app,components}/**/*.css', function(event) {
      gulp.start('build');
    });

    gulp.watch(options.src + '/{app,components}/**/*.js', function(event) {
      gulp.start('build');
    });
  });
};
