'use strict';

var ctrl = function($scope, apiService, notifyService, geoService, mapService, socketService){
	//apiService.ensureAccessNotLogin();

	this.mapSettings = {
		// center: [55.71670458662395, 37.654732686127815],
		// zoom: 5
	};
	// this.markers = [];
	this.inited = false;
	this.map = null;
	this.circle = null;

	this.init = function(){
		
		apiService.getLocation(function(data){

			this.mapSettings = data;			
			this.inited = true;
			$scope.$apply();

		}.bind(this))
	}

	this.afterInit = function(data){
		console.log('afterInit', data);
		this.map = data;
	}

	this.mapClick = function(event){
		var coords = event.get('coords');
		var radius = 5000;
		mapService.removeCircle(this.circle, this.map);	
		this.circle = mapService.showCircle(coords, radius, this.map);

		socketService.emit('wait', {coords: coords})
	}

	this.closeMenu = function(){
		mapService.removeCircle(this.circle, this.map);	
		this.circle = null;
		socketService.emit('unwait');
	}

	socketService.on('photo', function(data){
		console.log('photo', data);
	});

	

};

angular.module('home', ['topSidebar', 'apiServiceModule', 'notifyServiceModule', 'geoServiceModule', 'mapServiceModule', 'socketServiceModule'])
	.controller('homeCtrl', [
		'$scope'
		,'apiService'
		,'notifyService'
		,'geoService'
		,'mapService'
		,'socketService'
		, ctrl
	]);