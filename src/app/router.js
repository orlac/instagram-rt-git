'use strict'

require('./services/pageService.js');
require('./home/homeCtrl.js');

angular.module('router', ['ui.router', 'home', 'pageServiceModule'])
	.config(function($locationProvider, $stateProvider, $urlRouterProvider) {

    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });
    // $urlRouterProvider.otherwise(function($injector, $location){
    //      $window.location = $location.absUrl();
    //  });
		// For any unmatched url, redirect to /state1
  	    $urlRouterProvider.otherwise("/404");
		//
		// Now set up the states
		$stateProvider
		.state('home', {
  			url: "/",
            template: require('ng-cache!./home/home.html'),
            controller: function(pageService) {
                pageService.setTitle('Home');
            }
  			
		})
		.state('404', {
  			url: "/404",
            template: require('ng-cache!./templates/404.html'),
            controller: function(pageService) {
                pageService.setTitle('Ничего не найдено');
            }
		});

	});