'use strict';
(function(window){

	var bower_components = '../bower_components';

	window.jQuery = window.$ = require('../../bower_components/jquery/dist/jquery.min.js');
	require('angular');
	require('ui-router');
	require('angular-touch');
	window._ = require('../../bower_components/lodash/lodash.min.js');
	window.Masonry = require('../../bower_components/masonry/dist/masonry.pkgd.js');
	window.moment = require('moment');
	require('../../bower_components/moment/locale/ru.js');

	require('../../bower_components/angular/angular-csp.css');
	
	// var jQuery = $;
	//window.jQuery = require("jquery");
	//window.$ = window.jQuery

	/* theme */
	require('../fonts1.css');
	require('../fonts2.css');
	require('../../bower_components/font-awesome/css/font-awesome.min.css');
	require('../devAid-v1.0/assets/css/styles.css');
	
	
	require('../../bower_components/bootstrap/dist/css/bootstrap.min.css');
	require('../../bower_components/bootstrap/dist/js/bootstrap.min.js');
	require('../../bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js');
	require('../../bower_components/angular-socket-io/socket.js');

	

	/* theme */
	// require('../squadfree/js/wow.min.js');
	require('../devAid-v1.0/assets/js/main.js');
	
	/* icons */
	// require('../iconpack.css');
	require('../common.css');



	// require('../../bower_components/bootstrap-material-design/dist/css/material.min.css');
	// require('../../bower_components/bootstrap-material-design/dist/css/material-fullpalette.min.css');
	// require('../../bower_components/bootstrap-material-design/dist/css/ripples.min.css');
	// require('../../bower_components/bootstrap-material-design/dist/js/ripples.min.js');
	// require('../../bower_components/bootstrap-material-design/dist/js/material.min.js');

	// $(function(){
	// 	$.material.init();	
	// });

	require('../../bower_components/ya-map/ya-map-2.1.min.js');
	require('./router.js');
	require('./top_sidebar/directive.js');
	require('./notifyService/notifyService.js');
	require('./services/apiService.js');
	require('./services/geoService.js');
	require('./services/mapService.js');
	require('./services/socketService.js');

	// require('./home/homeCtrl.js');
	

	var ng = angular.module('application1', [
		'router'
		, 'ngTouch'
		, 'ui.bootstrap'
		, 'topSidebar'
		, 'apiServiceModule'
		, 'notifyServiceModule'
		, 'geoServiceModule'
		, 'mapServiceModule'
		, 'socketServiceModule'
		, 'yaMap'
		, 'btford.socket-io'
		// , 'home'
	])
	.config(['socketConfigProvider', function (socketConfigProvider) {
	  	socketConfigProvider.setConfig(window.socketConfig);
	}]);

	angular.element(document).ready(function() {
	    angular.bootstrap(document, [ng.name]);
	});


})(window);




