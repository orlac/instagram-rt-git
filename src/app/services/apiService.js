'use strict';

angular.module('apiServiceModule', ['notifyServiceModule', 'ui.router', 'geoServiceModule'])
	.factory('apiService', ['$state', '$window', '$location', 'notifyService', 'geoService',/*'$state',*/ function($state, $window, $location, notifyService, geoService/*, $state*/){
		

		var publish = {

          ensureAccessNotLogin: function(){
            var me = this;
            this.getUser(function(){
                me.go('cabinetKeys')
            }, function(){});  
          },
          ensureAccess: function(ignoreTimeZone){
            var me = this;
            var data = {};
            if(ignoreTimeZone){
              data.tz_i = true;
            }
            this._get_user(data, null, function(err){ 
                // me.go( err.go || '/')
                //todo resolve sref
                me.goout( err.go || '/')
                notifyService.error( err.message || 'Доступ запрещен' )
            });  
          },

          _get_user: function(data, _ok, _err){
            var me = this;
            $.ajax({
              dataType: 'json',
              url: '/api/user/get',
              data: data,
              success: function(res){
                console.log('/api/user/get', res);
                if(_ok){
                  _ok(res);
                }
              },
              error: function(err){
                err = err.responseJSON || {};
                console.error('/api/user/get ', err);
                if(_err){
                  _err(err);
                }else{
                  notifyService.error( err.message || 'что-то пошло не так...' )
                }
              }
            });
          },

          getUser: function(_ok, _err){
            var me = this;
            me._get_user(null, _ok, _err);
          },

          getLocation: function(_ok, _err){
            var me = this;

            geoService.getLocation(function(location){
              if(!location){
                me.getApiLocation(function(data){
                    _ok(data);
                }, _err)
              }else{
                _ok({
                  center: [ location.coords.longitude, location.coords.latitude],
                  zoom: 10
                });
              }
            });
          },

          getApiLocation: function(_ok, _err){
            var me = this;
            $.ajax({
              dataType: 'json',
              url: '/api/map',
              success: _ok,
              error: function(err){
                if(_err){
                  _err(err);
                }else{
                  notifyService.error( err.message || 'что-то пошло не так...' );
                }
              }
            });
          },

          // getAuthServices: function(_ok, _err){
          //   var me = this;
          //   $.ajax({
          //     dataType: 'json',
          //     url: '/api/keys/get',
          //     success: _ok,
          //     error: function(err){
          //       if(_err){
          //         _err(err);
          //       }else{
          //         notifyService.error( err.message || 'что-то пошло не так...' );
          //       }
          //     }
          //   });
          // },

          // getUserFeeds: function(_ok){
          //   var me = this;
          //   $.ajax({
          //     dataType: 'json',
          //     url: '/api/user/feeds/get',
          //     success: _ok,
          //     error: notifyService.error
          //   });
          // },

          // getUserChannels: function(_ok){
          //   var me = this;
          //   $.ajax({
          //     dataType: 'json',
          //     url: '/api/user/channels/get',
          //     success: _ok,
          //     error: notifyService.error
          //   });
          // },

          // deleteUserChannel: function(id, _ok){
          //   var me = this;
          //   $.ajax({
          //     type: 'DELETE',
          //     dataType: 'json',
          //     data: {
          //       id: id
          //     },
          //     url: '/api/user/channels/delete',
          //     success: _ok,
          //     error: notifyService.error
          //   });
          // },

          // addFeed: function(data, _ok){
          //   var me = this;
          //   $.ajax({
          //     type: 'post',
          //     data: data,
          //     dataType: 'json',
          //     url: '/api/user/channels/add',
          //     success: _ok,
          //     error: notifyService.error
          //   });
          // },

          // saveUserFeed: function(files, data, _ok, _err){
          //   var me = this;
            
          //   // for(var i in data){
          //   //   formData.append(i, data[i]);  
          //   // }
          //   // formData.append('id', '123');
          //   // formData.append('form', JSON.stringify(data));
          //   // if(files.length > 0){
          //   //   formData.append('uploadFile', files[0]);
          //   // }
            
          //   $.ajax({
          //     type: 'POST',
          //     data: {feed: data},
          //     cache: false,
          //     dataType: 'json',
          //     url: '/api/user/publish/add',
          //     success: function(res){
          //       if(files.length > 0){
          //         var formData = new FormData();
          //         formData.append('uploadFile', files[0]);
          //         $.ajax({
          //           type: 'POST',
          //           data: formData,
          //           processData: false,
          //           contentType: false,
          //           cache: false,
          //           dataType: 'json',
          //           url: '/api/user/publish/image/'+res.id,
          //           success: _ok,
          //           error: _err || notifyService.error
          //         });      
          //       }else{
          //         _ok(res);
          //       }
          //     },
          //     error: _err || notifyService.error
          //   });
          // },

          // getUserWaitFeed: function(_ok, _err){
          //   var me = this;
          //   $.ajax({
          //     dataType: 'json',
          //     url: '/api/user/publish/wait',
          //     success: _ok,
          //     error: _err || notifyService.error
          //   });
          // },

          // getUserArchiveFeed: function(_ok, _err){
          //   var me = this;
          //   $.ajax({
          //     dataType: 'json',
          //     url: '/api/user/publish/archive',
          //     success: _ok,
          //     error: _err || notifyService.error
          //   });
          // },

          // getPublishSettings: function(_ok){
          //   var me = this;
          //   $.ajax({
          //     dataType: 'json',
          //     url: '/api/user/publish/settings',
          //     success: _ok,
          //     error: notifyService.error
          //   });
          // },

          // saveSettings: function(data, _ok){
          //   var me = this;
          //   $.ajax({
          //     type: 'post',
          //     data: data,
          //     dataType: 'json',
          //     url: '/api/user/settings',
          //     success: _ok,
          //     error: notifyService.error
          //   });
          // },

          // toggleUserFeed: function(id, _ok){
          //   var me = this;
          //   $.ajax({
          //     type: 'post',
          //     data: {id: id},
          //     dataType: 'json',
          //     url: '/api/user/publish/toggle',
          //     success: _ok,
          //     error: notifyService.error
          //   });
          // },

          // deleteUserFeed: function(id, _ok){
          //   var me = this;
          //   $.ajax({
          //     type: 'DELETE',
          //     data: {id: id},
          //     dataType: 'json',
          //     url: '/api/user/publish/delete',
          //     success: _ok,
          //     error: notifyService.error
          //   });
          // },

          goout: function(path){
            $window.location.href = path;
          },
          go: function(path){
            //todo костыльно это
            // scope.document.getElementById('router').go(path, {replace: true})
            // var href = $urlRouter.href( path );
            $state.go(path);
            // $window.location.href = path;
            // $location.path(path);
          }
      	};

      	return publish;
	}]);