'use strict';

angular.module('geoServiceModule', [])
	.factory('geoService', ['$window', function($window){
		
	    var publish = {

			getLocation: function(cb){
				if ($window.navigator.geolocation) {
			        $window.navigator.geolocation.getCurrentPosition(function(position){
			        	cb(position);
			        }, function(e){
			        	console.log(e);
			        	cb();
			        });
			    } else {
			        cb()
			    }
			}
      	};

      	return publish;
	}]);