'use strict';

angular.module('mapServiceModule', [])
	.factory('mapService', ['$window', function($window){
		
		var publish = {};
		var map=null;
		publish.setMap = function(_map){
			map = _map;
		}

	    publish.showCircle = function(coords, radius, map){
			// Создаем круг.
		    var myCircle = new ymaps.Circle([
		        // Координаты центра круга.
		        coords,
		        // Радиус круга в метрах.
		        radius
		    ], {
		        // Описываем свойства круга.
		        // Содержимое балуна.
		        // balloonContent: "Радиус круга - 10 км",
		        // Содержимое хинта.
		        // hintContent: "Подвинь меня"
		    }, {
		        // Задаем опции круга.
		        // Включаем возможность перетаскивания круга.
		        draggable: true,
		        // Цвет заливки.
		        // Последний байт (77) определяет прозрачность.
		        // Прозрачность заливки также можно задать используя опцию "fillOpacity".
		        fillColor: "#DB709377",
		        // Цвет обводки.
		        strokeColor: "#990066",
		        // Прозрачность обводки.
		        strokeOpacity: 0.8,
		        // Ширина обводки в пикселях.
		        strokeWidth: 1
		    });

		    console.log('myCircle', myCircle);
		    map.geoObjects.add(myCircle);
		    return myCircle;
      	};

      	publish.removeCircle = function(obj, map){
      		if(obj){
      			map.geoObjects.remove(obj);	
      		}
      	}

      	return publish;
	}]);