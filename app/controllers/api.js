var express = require('express'),
  router = express.Router(),
  //marko = require('marko'),
  db = require('../models');

module.exports = function (app) {
  app.use('/api/', router);
};

router.get('/map', function (req, res, next) {

    res.send({
		center: [ 42.1031271688029, 50.84571456032383],
		zoom: 9
    });
});
