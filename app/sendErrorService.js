var env = process.env.NODE_ENV || 'development';


var _buildSequelizeValidationError = function(err){
	var msq = {};
	msq.error = (err.message)? err.message : 'ошибка, что-то пошло не так...';
	if(err.errors){
		msq.errors = err.errors;
	}

	console.log('_buildSequelizeValidationError', err)
	if(env !== 'production'){
		msq.source = err;
	}

	return msq;
	// if(err.name === 'SequelizeValidationError'){
	// 	err.errors 
	// }else{
	// 	return err;
	// }
}


module.exports = {


	send: function(req, code, err){
		var _msg = _buildSequelizeValidationError(err);
		console.log('_msg',_msg)
		req.send(code, _msg);
	},

	getErrObject: function(err){
		return _buildSequelizeValidationError(err);
	}

}